from klasy import Car

def test_drive_increases_car_mileage():
    test_car = Car(brand="Mazda", color="Green", pr_year="2019")
    test_car.drive(100)
    test_car.drive(100)
    assert test_car.mileage == 200