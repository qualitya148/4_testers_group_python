friend_first_name = "Ania"
friend_age = 40
friend_number_of_pets = 3
friend_has_driving_license = True
friendship_length_in_years = 18.5

print("My friend's first name:", friend_first_name)
print("My friend's age:", friend_age)
print("My friend's number of pets:", friend_number_of_pets)
print("My friend has driving license:", friend_has_driving_license)
print("How many years we've been friends:", friendship_length_in_years)
