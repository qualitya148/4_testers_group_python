## def - funkcja
## po spacji jest nazwa funkcji
## nakońcu bez spacji jest ():
## w nawiasie podajemy ARGUMENTY funkcji
## jak robimy definicjęfunkcji to klikając Enter przechodzi do nowej linii z tabulatorem tzn że jesteśmy w ciele funkcji. Żeby wyjść klikamy backspace

def print_a_car_brand_name():
    print("Honda")


print_a_car_brand_name()


###
def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


print_given_number_multiplied_by_3(10)


####
## return oznacza że funkcja zwraca wartosć ale jej nie wyświetli
def calcualte_area_of_a_circle(radius):
    return 3.14 * radius ** 2


## użycie return w funkcji kończy działanie funkcji

calcualte_area_of_a_circle(10)

##zeby wyświetlić wartość zmiennej trzeba do niej przypisać funkcję
area_of_a_circle_with_radius_10 = calcualte_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)


def calculate_area_of_a_traingle(a, h):
    return 1 / 2 * a * h

area_traingle = calculate_area_of_a_traingle(5, 5)
print(area_traingle)


def add(a, b):
    return a + b

if __name__ == '__main__':
    number1 = 5
    number2 = 6
    sum_1_2 = add(number1, number2)
    print(sum_1_2)


#w przykłądzei z dodatkowych filmów pokazałeś taki zapis i wykonanie funkcji
def calculate_area_of_a_traingle(a, h):
    return 1 / 2 * a * h

area_traingle = calculate_area_of_a_traingle(5, 5)
print(area_traingle)

#z kolei w prezentacji jest tak - które jest "bardziej" poprawne?
def add(a, b):
    return a + b

if __name__ == '__main__':
    number1 = 5
    number2 = 6
    sum_1_2 = add(number1, number2)
    print(sum_1_2)

def volume_of_cuboid(a,b,c):
    return a * b * c

result = volume_of_cuboid(3,5,7)
print(result)


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9/5 + 32
fahrenheit_temperature = convert_celsius_to_fahrenheit(20)
print(fahrenheit_temperature)