def generate_emails(first_name, lastname):
    return f"{first_name.lower()}.{lastname.lower()}@4testers.pl"


if __name__ == '__main__':
    user1_first_name = "Janusz"
    user1_lastname = "Nowak"
    user2_first_name = "Barbara"
    user2_lastname = "Kowalska"
    user_email_1 = generate_emails(user1_first_name, user2_lastname)
    user_email_2 = generate_emails(user2_first_name, user2_lastname)
    print(user_email_1)
    print(user_email_2)


import random
from datetime import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Funkcja do generowania danych
def generate_person():
    gender = random.choice(['male', 'female'])
    if gender == 'male':
        firstname = random.choice(male_fnames)
    else:
        firstname = random.choice(female_fnames)

    lastname = random.choice(surnames)
    country = random.choice(countries)
    email = f"{firstname.lower()}.{lastname.lower()}@example.com"
    age = random.randint(5, 45)
    adult = age >= 18
    birth_year = datetime.now().year - age

    return {
        'firstname': firstname,
        'lastname': lastname,
        'country': country,
        'email': email,
        'age': age,
        'adult': adult,
        'birth_year': birth_year
    }

# Utwórz listę 10 słowników z danymi
people_list = [generate_person() for _ in range(10)]

# Wygeneruj opisy i wydrukuj
for person in people_list:
    description = f"Hi! I'm {person['firstname']} {person['lastname']}. " \
                  f"I come from {person['country']} and I was born in {person['birth_year']}."
    print(description)
    print(people_list)