first_name = "Tomek"
last_name = "Radomiak"
email = "t.radomiak@gmail.com"


print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")
my_bio = "Mam na imię " + first_name + ". " + "Moje nazwisko to " + last_name + ". " + "Mój email to " + email + "."
print(my_bio)

#### F-STRING #####
my_bio_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}"

print(my_bio_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}")

### Algebra ###

area_of_a_circle_with_radius_5 = 3.14 * 5 ** 2   ## 5 do potęgi 2 można zapisać tak 5 ** 2
print(area_of_a_circle_with_radius_5)

circumference_of_a_circle_with_radius_5 = 2 * 3.14 * 5
print(circumference_of_a_circle_with_radius_5)

##inny zapis wartości promienia = 5

circle_radius = 5
print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius_5)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius_5)