import datetime


class Car:
    def __init__(self, brand, color, pr_year):
        self.brand = brand
        self.color = color
        self.pr_year = pr_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        return datetime.datetime.now().year - self.pr_year

    def repaint(self,color):
        self.color = color


if __name__ == '__main__':
    car1 = Car(brand="Mazda", color="Green", pr_year="2019")
    car2 = Car(brand="Polonez", color= "Orange", pr_year="1994")
    car3 = Car(brand="Peugeot", color= "White", pr_year="2017")




class Airplane:
    def __init__(self, name, total_seats):
        self.name = name
        self.total_seats = total_seats
        self.total_distance = 0
        self.occupied_seats = 0

    def fly(self, distance):
        self.total_distance += distance

    def is_service_required(self):
        return self.total_distance > 10000

    def board_passengers(self, number_of_passengers):
        if self.occupied_seats + number_of_passengers <= self.total_seats:
            self.occupied_seats += number_of_passengers
            print(f"Boarded {number_of_passengers} passengers on {self.name}.")
        else:
            print(f"Not enough available seats on {self.name}.")

    def get_available_seats(self):
        return self.total_seats - self.occupied_seats


if __name__ == '__main__':
    airplane1 = Airplane(name="Boeing 747", total_seats=400)
    airplane2 = Airplane(name="Airbus A320", total_seats=200)

    airplane1.fly(12000)
    airplane2.fly(8000)

    print(f"{airplane1.name} requires service: {airplane1.is_service_required()}")
    print(f"{airplane2.name} requires service: {airplane2.is_service_required()}")

    airplane1.board_passengers(300)
    airplane2.board_passengers(150)

    print(f"Available seats on {airplane1.name}: {airplane1.get_available_seats()}")
    print(f"Available seats on {airplane2.name}: {airplane2.get_available_seats()}")
