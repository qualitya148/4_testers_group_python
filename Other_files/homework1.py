##Napisz funkcję, która będzie generować adresy email w domenie naszej firmy -> “4testers.pl”.
##Email ma formę “imie.nazwisko@4testers.pl”
##Niech funkcja przyjmuje imię i nazwisko i zwraca utworzony email.
##Wydrukuj emaile dla użytkowników:
##Janusz Nowak
##Barbara Kowalska


#def generate_emails(name, lastname):
   # return f"{name.lower()}.{lastname.lower()}@4testers.pl"

    #user_email = f"{name.lower()}.{lastname.lower()}@4testers.pl"
    #print(user_email)

#if __name__ == '__main__':
    #generate_emails("Janusz", "Nowak")
    #generate_emails("Barbara", "Kowalska")

#a czy to zadanie da sięzrobićz return i potem wydrukować i jak to zrobić? Return zrobiłbym jak poniżej, ale jak to wydrukować? Jak podstawić zmienne?
#Powinny być
   # user1_name = "Janusz"
   # user1_lastname = "Nowak"
   # user2_name = "Barbara"
   # user2_lastname = "Kowalska"
   # user_email1 = generate_emails(user1_name, user2_lastname)
  #  user_email2 = generate_emails(user2_name, user2_lastname)
 #   print(user_email1)
#    print(user_email2)

def generate_emails(first_name, lastname):
    return f"{first_name.lower()}.{lastname.lower()}@4testers.pl"


if __name__ == '__main__':
    janusz_email = generate_emails("Janusz", "Nowak")
    barbara_email = generate_emails("Barbara", "Kowalska")
    print(janusz_email)
    print(barbara_email)