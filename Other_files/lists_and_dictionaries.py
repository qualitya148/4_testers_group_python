## listy
## lista ma elementy w nawiasach []
## elementy mająindexy, które numerujemy od 0

shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'liquid']
##tu będa pomarańcze

print(shopping_list[0])

##tu będzie liquid (czyli oznaczamy liczenie od prawej, ale zawsze od -1
print(shopping_list[-1])

## tu będą potatoes
print(shopping_list[-2])

#dodanie pozycji do listy
shopping_list.append('lemons')
print(shopping_list)
## teraz zwróci lemons, które dodaliśmy za pomocą append
print(shopping_list[-1])

##liczenie ilości elementów listy. Policzy że jest 6 bo dolicza lemons, które dodaliśmy za pomocą append
number_of_items_to_by = len(shopping_list)

print(number_of_items_to_by)


### wycinanie z listy zakresu
##bierzemy pierwsze 3 elementy. w nawiasach kwadratowych podajemy zakres indexów. 0 jako pierwszy i ostatni +1 bo python zawsze odejmuje 1 od ostatniego elementu
first_three_shoping_items = shopping_list[0:3]
print(first_three_shoping_items)


#######SŁOWNIKI#####
##słowniki zawierają klucze i wartości
animal = {
    "name": "Burek",
    "kind": "Dog",
    "age": 7,
    "male": True
}

#pobranie wartości po kluczu
dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal["name"]
print("Dog name:", dog_name)

##zmiana wartości klucza w słowniku
animal["age"] = 10

## dodanie warości
animal["owner"] = "Staszek"


print(animal)
