if __name__ == '__main__':
    my_friend = {
        "first_name": "Ania",
        "age": 45,
        "hobbies": ["books", "swimming"]
    }

    # zmieniamy imię przyjaciela
    my_friend["name"] = "John"
    my_friend["age"] = my_friend["age"] + 1
    # inny zapis powyższego
    my_friend["age"] += 1
    #dodanie hobby
    my_friend["hobbies"].append("archery")

    print(my_friend)





import string

def generate_login_data(email):
   # losowe hasło
    password =string.ascii_letters + string.digits + string.punctuation

    # słownik zawierający dane do logowania
    login_data = {
        'email': email,
        'password': password
    }

    return login_data

# użycie funkcji
user_email = "tomek@gmail.com"
generated_data = generate_login_data(user_email)
print("Login data:")
print("Email:", generated_data['email'])
print("Password:", generated_data['password'])